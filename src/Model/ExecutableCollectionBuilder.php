<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class ExecutableCollectionBuilder
{
    /**
     * @var Lister
     */
    protected $lister;

    /**
     * @var ExecutableFactory
     */
    protected $factory;

    /**
     * @var IconFactory
     */
    protected $iconFactory;

    public function __construct(Lister $lister, ExecutableFactory $factory, IconFactory $iconFactory)
    {
        $this->lister = $lister;
        $this->factory = $factory;
        $this->iconFactory = $iconFactory;
    }

    /**
     * @param Config $config
     *
     * @return ExecutableCollection
     *
     * @throws ExecutableCollectionBuildException
     */
    public function build(Config $config)
    {
        try {
            $executables = new ExecutableCollection();

            $executableConfigs = $config->getExecutableConfigs();

            foreach ($this->lister->getList($config) as $file) {
                $path = new RelativePath($file->getPath());
                $uri = $path = $path->getPath();
                $name = basename($path);
                $icon = null;

                if (array_key_exists($path, $executableConfigs)) {
                    $executableConfig = $executableConfigs[$path];
                    $uri = $executableConfig->getUri();
                    $path = $executableConfig->getPath();
                    $name = $executableConfig->getName();
                    $icon = $executableConfig->getIcon();
                }

                if (array_key_exists($path, $executableConfigs) && $executableConfigs[$path]->getUri()->isVhost()) {
                    $executables->addVhost(
                        $this->factory->createVhost($uri, $name, $icon)
                    );
                } elseif (is_dir($path)) {
                    $executables->addDirectory(
                        $this->factory->createDirectory($uri, $name, $icon)
                    );
                } elseif (is_file($path)) {
                    if ($icon === null) {
                        $info = new \finfo();
                        $mimetype = $info->file($path, FILEINFO_MIME_TYPE);
                        if ($mimetype == 'text/plain') {
                            $icon = $this->iconFactory->createPlainTextScriptIcon();
                        } elseif ($mimetype == 'application/octet-stream') {
                            $icon = $this->iconFactory->createBinaryScriptIcon();
                        }
                    }

                    $executables->addScript(
                        $this->factory->createScript($uri, $name, $icon)
                    );
                }
            }

            return $executables;
        } catch (ListException $exception) {
        }

        throw new ExecutableCollectionBuildException(null, null, $exception);//@todo

        //@todo to delete

        $path = $config->getParameter('ROOT_DIR').'..';

        $executables = new ExecutableCollection();

        $excludedPaths = array_map(function ($exclusionPath) use ($path) {
            return "$path/$exclusionPath";
        }, $config->getExcludedPaths());

        $configs = [];
        foreach ($config->getExecutableConfigs() as $executableConfig) {
            $configs["$path/".$executableConfig->getPath()] = $executableConfig;
        }

        foreach (glob("$path/*") as $node) {
            if (in_array($node, $excludedPaths)) {
                continue;
            }

            $uri = $path = RelativePath::cleanPath($node);
            $name = basename($node);
            $icon = null;
            if (array_key_exists($node, $configs)) {
                $uri = $configs[$node]->getUri();
                $path = $configs[$node]->getPath();
                $name = $configs[$node]->getName();
                $icon = $configs[$node]->getIcon();
            }

            if (array_key_exists($node, $configs) && $configs[$node]->getUri()->isVhost()) {
                $executables->addVhost(
                    $this->factory->createVhost($uri, $name, $icon)
                );
            } elseif (is_dir($node)) {
                $executables->addDirectory(
                    $this->factory->createDirectory($uri, $name, $icon)
                );
            } elseif (is_file($node)) {
                if ($icon === null) {
                    $info = new \finfo();
                    $mimetype = $info->file($node, FILEINFO_MIME_TYPE);
                    if ($mimetype == 'text/plain') {
                        $icon = $this->iconFactory->createPlainTextScriptIcon();
                    } elseif ($mimetype == 'application/octet-stream') {
                        $icon = $this->iconFactory->createBinaryScriptIcon();
                    }
                }
                $executables->addScript(
                    $this->factory->createScript($uri, $name, $icon)
                );
            }
        }
    }
}
