<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class Script implements ExecutableInterface
{
    /**
     * @var string
     */
    protected $filename;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Icon
     */
    protected $icon;

    public function __construct($filename, $name, Icon $icon)
    {
        $this->filename = $filename;
        $this->name = $name;
        $this->icon = $icon;
    }

    /**
     * {@inheritdoc}
     */
    public function getUri()
    {
        return $this->filename;
    }

    /**
     * {@inheritdoc}
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }
}
