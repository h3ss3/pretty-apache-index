<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class FileTransformer
{
    /**
     * @param array $data
     *
     * @return File
     */
    public function transform(array $data)
    {
        $file = new File();

        $file->setType($data['type']);
        $file->setPath($data['path']);
        if (array_key_exists('size', $data)) {
            $file->setSize($data['size']);
        }
        $file->setBasename($data['basename']);
        if (array_key_exists('extension', $data)) {
            $file->setExtension($data['extension']);
        }
        $file->setFilename($data['filename']);

        return $file;
    }

    /**
     * @todo
     *
     * @param File $file
     *
     * @return array
     */
    public function reverseTransform(File $file)
    {
    }
}
