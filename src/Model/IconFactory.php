<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class IconFactory
{
    const DEFAULT_DIRECTORY_IMG = 'src/Resources/public/img/';

    /**
     * @var Normalizer
     */
    protected $normalizer;

    /**
     * @var string
     */
    protected $rootDir;

    public function __construct(Normalizer $normalizer, $rootDir)
    {
        $this->normalizer = $normalizer;
        $this->rootDir = $rootDir;
    }

    /**
     * @param string $path
     *
     * @return Icon
     */
    public function createIcon($path)
    {
        $path = new RelativePath($this->rootDir.'/'.$path);
        $path = $path->getPath();

        return new Icon($path, $this->normalizer->normalize($path));
    }

    /**
     * @return Icon
     */
    public function createDefaultScriptIcon()
    {
        return $this->createIcon(self::DEFAULT_DIRECTORY_IMG.'script.png');
    }

    /**
     * @return Icon
     */
    public function createBinaryScriptIcon()
    {
        return $this->createIcon(self::DEFAULT_DIRECTORY_IMG.'mimetype-binary.png');
    }

    /**
     * @return Icon
     */
    public function createPlainTextScriptIcon()
    {
        return $this->createIcon(self::DEFAULT_DIRECTORY_IMG.'mimetype-plaintext.png');
    }

    /**
     * @return Icon
     */
    public function createDefaultVhostIcon()
    {
        return $this->createIcon(self::DEFAULT_DIRECTORY_IMG.'vhost.png');
    }

    /**
     * @return Icon
     */
    public function createDefaultDirectoryIcon()
    {
        return $this->createIcon(self::DEFAULT_DIRECTORY_IMG.'directory.png');
    }
}
