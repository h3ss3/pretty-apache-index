<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class PathTopLevelNotReachableException extends \Exception
{
    public function __construct($path, $piecePath, $number)
    {
        parent::__construct("Cannot reach level '$piecePath' after slash #$number in '$path'");
    }
}
