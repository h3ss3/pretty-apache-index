<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
interface ExecutableInterface
{
    /**
     * Return URI of the executable, can be relative (for a script or directory)
     * or absolute (for a local site having its own vhost).
     *
     * @return string
     */
    public function getUri();

    /**
     * Return path of the icon to display for the executable.
     *
     * @return Icon
     */
    public function getIcon();

    /**
     * Return the name to display.
     *
     * @return string
     */
    public function getName();
}
