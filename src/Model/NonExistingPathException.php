<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class NonExistingPathException extends \Exception
{
    public function __construct($path)
    {
        parent::__construct("Path '$path' does not exist");
    }
}
