<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class UnsupportedAbsolutePathException extends \Exception
{
    public function __construct($path)
    {
        parent::__construct("Absolute path '$path' is not supported");
    }
}
