<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class Normalizer
{
    /**
     * @param string $path
     *
     * @return string
     */
    public function normalize($path)
    {
        $path = preg_replace('#[^a-zA-Z0-9_\.]#', '_', basename($path));
        $path = preg_replace('#_+#', '_', $path);
        $path = preg_replace('#^_#', '', $path);
        $path = preg_replace('#_$#', '', $path);

        return $path;
    }
}
