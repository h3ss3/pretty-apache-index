<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class ExecutableCollection
{
    /**
     * @var array
     */
    protected $scripts;

    /**
     * @var array
     */
    protected $directories;

    /**
     * @var array
     */
    protected $vhosts;

    public function __construct()
    {
        $this->scripts = [];
        $this->directories = [];
        $this->vhosts = [];
    }

    /**
     * @param Script $script
     *
     * @return self
     */
    public function addScript(Script $script)
    {
        $this->scripts[] = $script;

        return $this;
    }

    /**
     * @param Directory $directory
     *
     * @return self
     */
    public function addDirectory(Directory $directory)
    {
        $this->directories[] = $directory;

        return $this;
    }

    /**
     * @param Vhost $vhost
     *
     * @return self
     */
    public function addVhost(Vhost $vhost)
    {
        $this->vhosts[] = $vhost;

        return $this;
    }

    /**
     * Gets the value of scripts.
     *
     * @return array
     */
    public function getScripts()
    {
        return $this->scripts;
    }

    /**
     * Gets the value of directories.
     *
     * @return array
     */
    public function getDirectories()
    {
        return $this->directories;
    }

    /**
     * Gets the value of vhosts.
     *
     * @return array
     */
    public function getVhosts()
    {
        return $this->vhosts;
    }
}
