<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class Uri
{
    /**
     * @var string
     */
    protected $uri;

    public function __construct($uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @return bool
     */
    public function isVhost()
    {
        return substr($this->uri, 0, 7) == 'http://' || substr($this->uri, 0, 8) == 'https://';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->uri;
    }
}
