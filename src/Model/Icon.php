<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class Icon
{
    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $alt;

    /**
     * @var string
     */
    protected $title;

    public function __construct($path, $alt = null, $title = null)
    {
        $this->path = $path;
        $this->alt = $alt;
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
