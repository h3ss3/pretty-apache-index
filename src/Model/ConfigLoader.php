<?php

namespace Model;

use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Parser;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class ConfigLoader
{
    /**
     * @var Parser
     */
    protected $parser;

    /**
     * @var IconFactory
     */
    protected $iconFactory;

    public function __construct(Parser $parser, IconFactory $iconFactory)
    {
        $this->parser = $parser;
        $this->iconFactory = $iconFactory;
    }

    /**
     * @param string $file
     *
     * @return Config
     *
     * @throws ConfigLoadingException
     */
    public function parse($file)
    {
        try {
            $config = new Config();

            $parsedConfigs = $this->parser->parse(file_get_contents($file));

            $parametersPart = $parsedConfigs['parameters'];
            $configPart = $parsedConfigs['config'];

            //section: parameters
            $config->addParameters($parametersPart);

            //section: config.excluded_paths
            if (array_key_exists('excluded_paths', $configPart) && $configPart['excluded_paths'] !== null) {
                $config->setExcludedPaths($configPart['excluded_paths']);
            }

            //section: config.customized_paths
            if (array_key_exists('customized_paths', $configPart) && $configPart['customized_paths'] !== null) {
                foreach ($configPart['customized_paths'] as $parsedConfig) {
                    $uri = new Uri($this->getFromConfig($parsedConfig, 'uri'));
                    $name = $this->getFromConfig($parsedConfig, 'name');
                    $icon = null;

                    if (array_key_exists('icon', $parsedConfig)) {
                        $icon = $this->iconFactory->createIcon(
                            $config->getParameter('icons_directory').'/'.$parsedConfig['icon']
                        );
                    }

                    $config->addExecutableConfig(new ExecutableConfig($parsedConfig['path'], $uri, $name, $icon));
                }
            }

            //section: config.file_extensions_restrict
            if (array_key_exists('file_extensions_restrict', $configPart) && $configPart['file_extensions_restrict'] !== null) {
                $config->setRestrictedExtensions($configPart['file_extensions_restrict']);
            }

            return $config;
        } catch (ParseException $exception) {
        }

        throw new ConfigLoadingException(null, null, $exception);//@todo
    }

    /**
     * @param array  $config
     * @param string $key
     *
     * @return string
     */
    protected function getFromConfig(array $config, $key)
    {
        $key = (array_key_exists($key, $config) ? $key : 'path');

        return $config[$key];
    }
}
