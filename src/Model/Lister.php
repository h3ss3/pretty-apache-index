<?php

namespace Model;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class Lister
{
    /**
     * @var FileTransformer
     */
    protected $transformer;

    public function __construct(FileTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * @return array array of File
     *
     * @throws ListException
     */
    public function getList(Config $config)
    {
        try {
            $prefix = new RelativePath($config->getParameter('ROOT_DIR'));
            $prefix = $prefix->parent()->getPath();
            $filesystem = new Filesystem(new Local($prefix, LOCK_EX, Local::SKIP_LINKS));

            $files = [];

            $contents = $filesystem->listContents();
            foreach ($contents as $content) {
                $content['path'] = $prefix.'/'.$content['path'];
                $file = $this->transformer->transform($content);

                $excluded = false;

                $excluded |= is_array($config->getRestrictedExtensions())
                    && in_array($file->getExtension(), $config->getRestrictedExtensions());

                foreach ($config->getExcludedPaths() as $excludedPath) {
                    $excluded |= $file->equalsTo($excludedPath);
                }

                if (!$excluded) {
                    $files[] = $file;
                }
            }

            return $files;
        } catch (PathTopLevelNotReachableException $exception) {
        } catch (UnsupportedAbsolutePathException $exception) {
        }

        throw new ListException(null, null, $exception);//@todo
    }
}
