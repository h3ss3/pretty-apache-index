<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class RelativePath
{
    /**
     * @var string
     */
    protected $path;

    /**
     * @var bool
     */
    protected $strict;

    /**
     * @param string $path
     * @param bool   $strict
     *
     * @throws UnsupportedAbsolutePathException
     */
    public function __construct($path, $strict = false)
    {
        if ($path == null || $path == '' || substr($path, 0, 1) == '/') {
            throw new UnsupportedAbsolutePathException($path);
        }

        $this->path = $path;
        $this->strict = $strict;
        $this->cleanPath();
    }

    public function append($subpath)
    {
        $path = new static($this->path.'/'.$subpath);
        $path->cleanPath();

        return $path;
    }

    public function parent()
    {
        $path = new static($this->path.'/..');
        $path->cleanPath();

        return $path;
    }

    public function getPath($withDirectoryDelimiter = false)
    {
        $path = $this->path;
        if ($withDirectoryDelimiter) {
            $path .= '/';
        }

        return $path;
    }

    /**
     * @return self
     *
     * @throws PathTopLevelNotReachableException
     */
    public function cleanPath()
    {
        $originalPieces = explode('/', $this->path);
        $pieces = [];

        $length = count($originalPieces);
        for ($i = 0; $i < $length; ++$i) {
            $current = $originalPieces[$i];
            if ($current == '' || $current == '.') {
                //do nothing
            } elseif ($current == '..') {
                if (count($pieces) == 0) {
                    if ($this->strict) {
                        throw new PathTopLevelNotReachableException($this->path, $current, $i);
                    } else {
                        $pieces[] = $current;
                    }
                } else {
                    array_pop($pieces);
                }
            } else {
                $pieces[] = $current;
            }
        }

        $this->path = implode('/', $pieces);

        if ($this->path == '') {
            $this->path = '.';
        }

        return $this;
    }
}
