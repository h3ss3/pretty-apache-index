<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class ConfigMissingSectionException
{
    public function __construct($configPath, $section)
    {
        parent::__construct("Missing section '$section' in $configPath");
    }
}
