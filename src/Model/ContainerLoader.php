<?php

namespace Model;

use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * @author Sylvain Carite <s.carite@xeonys.com>
 */
class ContainerLoader
{
    /**
     * @var string
     */
    protected $directory;

    public function __construct($directory)
    {
        $this->directory = $directory;
    }

    /**
     * @return ContainerBuilder
     *
     * @throws ContainerLoadingException
     */
    public function load()
    {
        try {
            $container = new ContainerBuilder();

            $directory = realpath($this->directory);
            $local = realpath('.');

            $rootDir = '';

            if (mb_strlen($directory) < mb_strlen($local))
                throw new ContainerLoadingException(
                    "Cannot extract a relative path from current web directory '$directory' ".
                    "by removing apache root directory '$local' ".
                    "because the first must be longer than the second!");

            if ($directory == $local)
                $rootDir = './';
            else {
                $relative = substr($directory, 0, strlen($local));
                if ($relative != $local)
                    throw new ContainerLoadingException(
                        "Cannot extract a relative path from current web directory '$directory' ".
                        "by removing apache root directory '$local' ".
                        "because the first must contains and begin by the second!");
                $rootDir = substr($directory, strlen($local)+1);
            }

            $rootDir = new RelativePath($rootDir);
            $container->setParameter('ROOT_DIR', $rootDir->parent()->getPath());

            return $container;
        } catch(PathTopLevelNotReachableException $exception) {
        } catch(UnsupportedAbsolutePathException $exception) {
        }

        throw new ContainerLoadingException(
            "Cannot extract a relative path from current web directory '$directory' ".
            "by removing apache root directory '$local'", null, $exception);
    }
}
