<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class ContainerLoadingException extends \Exception
{
}
