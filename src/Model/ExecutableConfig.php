<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class ExecutableConfig
{
    /**
     * @var string
     */
    protected $path;

    /**
     * @var Uri
     */
    protected $uri;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Icon|null
     */
    protected $icon;

    public function __construct($path, Uri $uri, $name, Icon $icon = null)
    {
        $this->path = $path;
        $this->uri = $uri;
        $this->name = $name;
        $this->icon = $icon;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return Uri
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Icon|null
     */
    public function getIcon()
    {
        return $this->icon;
    }
}
