<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class Vhost implements ExecutableInterface
{
    /**
     * @var string
     */
    protected $uri;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Icon
     */
    protected $icon;

    public function __construct($uri, $name, Icon $icon)
    {
        $this->uri = $uri;
        $this->name = $name;
        $this->icon = $icon;
    }

    /**
     * {@inheritdoc}
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * {@inheritdoc}
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }
}
