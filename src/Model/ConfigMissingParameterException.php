<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class ConfigMissingParameterException extends \Exception
{
    public function __construct($configPath, $parameter)
    {
        parent::__construct("Missing parameter '$parameter' in $configPath");
    }
}
