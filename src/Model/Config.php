<?php

namespace Model;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class Config
{
    /**
     * @var ParameterBagInterface
     */
    protected $parameterBag;

    /**
     * @var array
     */
    protected $excludedPaths;

    /**
     * @var array
     */
    protected $executableConfigs;

    /**
     * @var array
     */
    protected $restrictedExtensions;

    public function __construct()
    {
        $this->parameterBag = new ParameterBag();
        $this->excludedPaths = [];
        $this->executableConfigs = [];
        $this->restrictedExtensions = [];
    }

    /**
     * @param ParameterBagInterface $parameterBag
     *
     * @return self
     */
    public function mergeParameterBag(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag->add($parameterBag->all());

        return $this;
    }

    /**
     * @return ParameterBagInterface
     */
    public function getParameterBag()
    {
        return $this->parameterBag;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameterBag->all();
    }

    /**
     * @return mixed
     */
    public function getParameter($key)
    {
        return $this->parameterBag->get($key);
    }

    /**
     * @param array $parameters
     *
     * @return self
     */
    public function addParameters(array $parameters)
    {
        $this->parameterBag->add($parameters);

        return $this;
    }

    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return self
     */
    public function addParameter($key, $value)
    {
        $this->addParameters([$key => $value]);

        return $this;
    }

    /**
     * @return array array of ExecutableConfig
     */
    public function getExecutableConfigs()
    {
        return $this->executableConfigs;
    }

    /**
     * @return array array of string
     */
    public function getExcludedPaths()
    {
        return $this->excludedPaths;
    }

    /**
     * @param array $excludedPaths array of string
     *
     * @return self
     */
    public function setExcludedPaths(array $excludedPaths)
    {
        $this->excludedPaths = $excludedPaths;

        return $this;
    }

    /**
     * @param ExecutableConfig $executableConfig
     *
     * @return self
     */
    public function addExecutableConfig(ExecutableConfig $executableConfig)
    {
        $this->executableConfigs[$executableConfig->getPath()] = $executableConfig;

        return $this;
    }

    /**
     * @return array
     */
    public function getRestrictedExtensions()
    {
        return $this->restrictedExtensions;
    }

    /**
     * @param array $extensions
     *
     * @return self
     */
    public function setRestrictedExtensions(array $extensions)
    {
        $this->restrictedExtensions = $extensions;

        return $this;
    }
}
