<?php

namespace Model;

use Symfony\Component\Yaml\Parser;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class ConfigChecker
{
    /**
     * @var Parser
     */
    protected $parser;

    /**
     * @var string
     */
    protected $distPath;

    /**
     * @var string
     */
    protected $path;

    public function __construct(Parser $parser, $distPath, $path)
    {
        $this->parser = $parser;
        $this->distPath = $distPath;
        $this->path = $path;
    }

    /**
     * @throws NonExistingPathException
     * @throws ConfigMissingSectionException
     * @throws ConfigMissingParameterException
     */
    public function check()
    {
        //@todo
        return;

        $distContent = $this->parser->parse(file_get_contents($this->distPath));//throws ParseException
        if (!file_exists($this->path)) {
            throw new NonExistingPathException($this->path);
        }
        $content = $this->parser->parse(file_get_contents($this->path));

        //check parameters section
        if (!array_key_exists('parameters', $distContent)) {
            throw new ConfigMissingSectionException($this->path, 'parameters');
        }
        foreach ($distContent['parameters'] as $parameterName => $parameter) {
            if (!array_key_exists($parameterName, $content['parameters'])) {
                throw new ConfigMissingParameterException($this->path, $parameterName);
            }
        }

        //check config section
        if (!array_key_exists('config', $distContent)) {
            throw new ConfigMissingSectionException($this->path, 'config');
        }
        foreach ($distContent['config'] as $parameterName => $parameter) {
            if (!array_key_exists($parameterName, $content['config'])) {
                throw new ConfigMissingParameterException($this->path, $parameterName);
            }
        }

        //check excluded_paths section
        if (!array_key_exists('excluded_paths', $distContent)) {
            throw new ConfigMissingSectionException($this->path, 'excluded_paths');
        }

        //check configs section
        if (!array_key_exists('configs', $distContent)) {
            throw new ConfigMissingSectionException($this->path, 'configs');
        }

        //@todo
    }
}
