<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class File
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var int
     */
    protected $size;

    /**
     * @var string
     */
    protected $basename;

    /**
     * @var string
     */
    protected $extension;

    /**
     * @var string
     */
    protected $filename;

    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function getMimeType()
    {
        $info = new \finfo();

        return $info->file($this->path, FILEINFO_MIME_TYPE);
    }

    /**
     * @return bool
     */
    public function isFile()
    {
        return $this->type == 'file';
    }

    /**
     * @return bool
     */
    public function isDir()
    {
        return $this->type == 'dir';
    }

    /**
     * @param File|string
     *
     * @return bool
     */
    public function equalsTo($value)
    {
        if ($value instanceof self) {
            $filepath = $value->getPath();
        } else {
            $filepath = $value;
        }

        return $this->path == $filepath;
    }

    // GETTERS and SETTERS

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return self
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     *
     * @return self
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return string
     */
    public function getBasename()
    {
        return $this->basename;
    }

    /**
     * @param string $basename
     *
     * @return self
     */
    public function setBasename($basename)
    {
        $this->basename = $basename;

        return $this;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     *
     * @return self
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     *
     * @return self
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }
}
