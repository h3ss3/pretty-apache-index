<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class Directory implements ExecutableInterface
{
    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Icon
     */
    protected $icon;

    public function __construct($path, $name, Icon $icon)
    {
        $this->path = $path;
        $this->name = $name;
        $this->icon = $icon;
    }

    /**
     * {@inheritdoc}
     */
    public function getUri()
    {
        return $this->path;
    }

    /**
     * {@inheritdoc}
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }
}
