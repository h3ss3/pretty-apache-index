<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class ExecutableFactory
{
    /**
     * @var IconFactory
     */
    protected $iconFactory;

    public function __construct(IconFactory $iconFactory)
    {
        $this->iconFactory = $iconFactory;
    }

    /**
     * @param string      $filename
     * @param string|null $name
     * @param Icon|null   $icon
     *
     * @return Script
     */
    public function createScript($filename, $name = null, Icon $icon = null)
    {
        if ($icon === null) {
            $icon = $this->iconFactory->createDefaultScriptIcon();
        }

        if ($name === null) {
            $name = basename($filename);
        }

        return new Script($filename, $name, $icon);
    }

    /**
     * @param string      $uri
     * @param string|null $name
     * @param Icon|null   $icon
     *
     * @return Vhost
     */
    public function createVhost($uri, $name = null, Icon $icon = null)
    {
        if ($icon === null) {
            $icon = $this->iconFactory->createDefaultVhostIcon();
        }

        if ($name === null) {
            $name = $uri;
        }

        return new Vhost($uri, $name, $icon);
    }

    /**
     * @param string      $path
     * @param string|null $name
     * @param Icon|null   $icon
     *
     * @return Directory
     */
    public function createDirectory($path, $name = null, Icon $icon = null)
    {
        if ($icon === null) {
            $icon = $this->iconFactory->createDefaultDirectoryIcon();
        }

        if ($name === null) {
            $name = basename($path);
        }

        return new Directory($path, $name, $icon);
    }
}
