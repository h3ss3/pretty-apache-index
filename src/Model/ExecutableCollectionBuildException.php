<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class ExecutableCollectionBuildException extends \Exception
{
}
