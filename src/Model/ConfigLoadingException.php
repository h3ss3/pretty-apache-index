<?php

namespace Model;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class ConfigLoadingException extends \Exception
{
}
