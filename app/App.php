<?php

use Model\ConfigLoadingException;
use Model\ConfigMissingParameterException;
use Model\ConfigMissingSectionException;
use Model\ContainerLoader;
use Model\ContainerLoadingException;
use Model\ExecutableCollectionBuildException;
use Model\NonExistingPathException;
use Model\RelativePath;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * @author Hessé <sylvain.carite@gmail.com>
 */
class App
{
    public static function main(array $args = [])
    {
        try {
            //load container for dependencies
            $containerLoader = new ContainerLoader($args[0]);
            $container = $containerLoader->load();

            //load dependency injection services
            $servicesFile = new RelativePath($container->getParameter('ROOT_DIR'));
            $loader = new YamlFileLoader($container, new FileLocator($servicesFile->append("src/Resources/config/services/")->getPath()));
            $loader->load("services.yml");

            //check coherence of app/config/config.yml with .dist file
            $container->get('config_checker')->check();

            //load app/config/config.yml
            $configFile = new RelativePath($container->getParameter('ROOT_DIR'));
            $config = $container->get('config_loader')->parse($configFile->append("app/config/config.yml")->getPath());

            //merge config parameters in $config parameters
            $config->mergeParameterBag($container->getParameterBag());

            //add global vars in twig templating
            $container->get('templating')->addGlobal('ROOT_DIR', $container->getParameter('ROOT_DIR'));

            //browse apache site index and create an array of Executable objects to render
            $executables = $container->get('executable_collection_builder')->build($config);

            //rendering
            echo $container->get('templating')->render('index.html.twig', [
                'executables' => $executables,
            ]);

            return;
        } catch(ContainerLoadingException $exception) {//from ContainerLoader
        } catch(NonExistingPathException $exception) {//from ConfigChecker
        } catch(ConfigMissingSectionException $exception) {//from ConfigChecker
        } catch(ConfigMissingParameterException $exception) {//from ConfigChecker
        } catch(ConfigLoadingException $exception) {//from ConfigLoader
        } catch(ExecutableCollectionBuildException $exception) {//from ExecutableCollectionBuilder
        } catch(\Exception $exception) {
            echo "<pre>UNRECOGNIZED EXCEPTION: ".get_class($exception)." - ".$exception->getMessage().' - '.$exception->getTraceAsString();

            return;
        }

        self::renderException($exception);
    }

    public static function renderException(\Exception $exception)
    {
        while( $exception !== null ) {
            echo get_class($exception).": ".$exception->getMessage()."<br />".$exception->getTraceAsString()."<br /><br />";
            $exception = $exception->getPrevious();
        }
    }
}
