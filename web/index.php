<?php

//autoload dependencies (class loader, twig)
require_once __DIR__.'/../vendor/autoload.php';

//load app main class
require_once __DIR__.'/../app/App.php';

App::main([__DIR__]);
