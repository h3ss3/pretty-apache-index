# Pretty Apache Index

## Goal

Get a pretty and customizable index page for apache sites directory. This project is not a symfony project but uses many symfony components and a similar architecture. This aims to be as light as possible.

## Requirements

- PHP 5.x
- Composer
- Npm

## Install

### Step 1: get project

```shell
git clone git@gitlab.com:h3ss3/pretty-apache-index.git
composer install
npm install
./node_modules/.bin/bower install
```

### Step 2: configuration

- Copy file `app/config/config.yml.dist` to `app/config/config.yml`. (You have a complete example in `app/config/config.yml.example`)
- Modify `parameters` section to adapt to your project.
- If you have some files or directories to hide, put their simple name in `excluded_paths` section.
- If you have some virtual host and/or want to customize some scripts, directories, put them in `configs` section as arrays like :
`{ path: <simple name>, uri: <uri for vhost>, name: <name to display>, icon: <icon>}` (only path is required). For more information, see Configuration file.

### Step 3: link to your apache sites directory index

Create a symlink to your apache index. For example if you use the default apache sites directory `/var/www` and if you clone pretty-apache-index in it, then create the following link (use sudo if you do not own apache sites directory):
```shell
ln -s /var/www/pretty-apache-index/web/index.php /var/www/index.php
```

## Configuration file

```yml
parameters:
    icons_directory: "web/img" #directory containing your own icons

excluded_paths: #simple list of scripts/directories to exclude from index page
    - 'toto.php'
    - 'project-test'

configs: #customized scripts/directories/vhosts
    - { path: 'phpPgAdmin-5.1', uri: 'http://pgadmin.local', name: 'PhpPgAdmin', icon: 'phppgadmin.png'}
    - { path: 'phpMyAdmin-4.4.10-all-languages', uri: 'http://myadmin.local', name: 'PhpMyAdmin'}
    - { path: 'cms1', icon: "cms.png"}
    - { path: 'phpinfo.php', icon: "php.png"}
```

Note: in `configs` section, for every array, only `path` is required:
- `path` must be a script or directory (no sub directory or sub script) directly in apache sites directory with no ending slash.
- `uri` contains your virtual host ; if no uri, path will be used instead.
- `name` is the display name ; if no name, path basename will be used instead.
- `icon` is your customized icon ; if no icon, default icon will be used instead.
